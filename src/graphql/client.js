import { ApolloClient, InMemoryCache } from '@apollo/client';
import { offsetLimitPagination } from '@apollo/client/utilities';
import { persistCache, SessionStorageWrapper } from 'apollo3-cache-persist';

const cache = new InMemoryCache({
    typePolicies: {
        Query: {
            fields: {
                pokemon_v2_pokemonspecies: offsetLimitPagination(),
            }
        },
        pokemon_v2_type: {
            keyFields: ["name"]
        }
    }
});

persistCache({
    cache,
    storage: new SessionStorageWrapper(window.sessionStorage),
});

export const client = new ApolloClient({
    uri: 'https://beta.pokeapi.co/graphql/v1beta',
    cache,
});