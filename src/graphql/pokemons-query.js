import gql from 'graphql-tag';

export const PokemonsQuery = {
    GET_POKEMONS: gql`
    query getPokemons($limit: Int, $offset: Int) {
        species: pokemon_v2_pokemonspecies(limit: $limit, offset: $offset, order_by: {id: asc}) {
            id
            name
            pokemons: pokemon_v2_pokemons {
                types: pokemon_v2_pokemontypes {
                    type: pokemon_v2_type {
                        name
                    }
                }
            }
        }
        species_aggregate: pokemon_v2_pokemonspecies_aggregate {
            aggregate {
                count
            }
        }
    }`,
    GET_POKEMON_DETAIL: gql`
    query getPokemon($name: String = "") {
        pokemon: pokemon_v2_pokemonspecies(limit: 1, where: {name: {_eq: $name}}) {
            id
            name
            description: pokemon_v2_pokemonspeciesflavortexts(limit: 1, where: {pokemon_v2_language: {name: {_eq: "en"}}}) {
                flavor_text
            }
            gender_rate
            hatch_counter
            egg_groups: pokemon_v2_pokemonegggroups {
                group: pokemon_v2_egggroup {
                    name
                }
            }
            pokemons: pokemon_v2_pokemons {
                id
                name
                height
                weight
                abilities: pokemon_v2_pokemonabilities {
                    ability: pokemon_v2_ability {
                        name
                    }
                }
                types: pokemon_v2_pokemontypes {
                    type: pokemon_v2_type {
                        name
                    }
                }
                stats: pokemon_v2_pokemonstats {
                    base_stat
                    stat: pokemon_v2_stat {
                    name
                    }
                }
            }
            evolutions: pokemon_v2_evolutionchain {
                species: pokemon_v2_pokemonspecies(order_by: {order: asc}) {
                    id
                    name
                    evolves_from_species_id
                    evolutions: pokemon_v2_pokemonevolutions {
                        by_held_item: pokemonV2ItemByHeldItemId {
                            name
                        }
                        evolution_trigger: pokemon_v2_evolutiontrigger {
                            name
                        }
                        gender: pokemon_v2_gender {
                            name
                        }
                        item: pokemon_v2_item {
                            name
                        }
                        location: pokemon_v2_location {
                            name
                        }
                        min_affection
                        min_beauty
                        min_happiness
                        min_level
                        move: pokemon_v2_move {
                            name
                        }
                        time_of_day
                    }
                }
            }
        }
    }`,
}