import styled from '@emotion/styled';
import { css } from '@emotion/react';

export const MobileView = styled.div`
    max-width: 576px;
    margin: 0 auto; 
`

export const alignCenter = css`
    display: block;
    margin: 0 auto;
`

export const stickyTop = css`
    top: 0;
    position: sticky;
`

export const roundedBox = css`
    border-radius: 20px 20px 0 0;
`

export const roundedBorder = css`
    border-radius: 1.25em;
`

export const hoverAnimation = css`
    &:hover {
        transform: translateY(-0.4em);
        transition: 0.2s all ease-in-out;
    }
`

export const semiBold = css`
    font-weight: 500;
`

export const greyText = css`
    color: #718096;
`

export const textUppercase = css`
    text-transform: uppercase;
`

export const spaceContentBetween = css`
    display: flex;
    justify-content: space-between;
`