import { css } from '@emotion/react';

export const navigationLink = css`
    letter-spacing: .5px;
    &:focus { background-color: initial !important; }
`