import { css, keyframes } from '@emotion/react';

const spinnerAnimation = keyframes`
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
`

export const spinner = css`
    display: inline-block;
    width: 60px;
    height: 60px;
    &::after {
        content: " ";
        display: block;
        width: 50px;
        height: 50px;
        margin: 5px;
        border-radius: 50%;
        border: 5px solid #fff;
        border-color: #3273dc transparent #3273dc transparent;
        animation: ${spinnerAnimation} 1.2s linear infinite;
    }
`