import { Navbar } from 'react-bulma-components';
import { NavLink } from 'react-router-dom';
import { stickyTop } from '../styles/global';
import { navigationLink } from '../styles/navigation';

export default function TopNavigation() {
    return (
        <Navbar color="link" css={stickyTop}>
            <Navbar.Brand>
                <NavLink to="/" css={navigationLink} className="navbar-item has-text-weight-semibold">
                    <img src="/assets/image/iconfinder__Pokeball_1337537.png" width="28" height="28" alt="Pokedex: Gotta catch 'em all" className="mr-1"/>
                    Pokedex
                </NavLink>
            </Navbar.Brand>
        </Navbar>
    )
}