import React, { useEffect, useState } from 'react';
import { Container, Hero, Heading, Box, Columns, Progress } from 'react-bulma-components';
import { Offline } from "react-detect-offline";
import { useLazyQuery } from '@apollo/client';

import { alignCenter,  greyText, roundedBox, semiBold, spaceContentBetween, textUppercase } from './styles/global';
import AutoDismiss from './custom/AutoDismiss';
import { PokemonsQuery } from '../graphql/pokemons-query';

export default function PokemonDetail(props) {
    const pokemonName = props.match.params.pokemonName
    const [getPokemonDetail, { loading, error, data: { pokemon = [] } = {} }] = useLazyQuery(PokemonsQuery.GET_POKEMON_DETAIL, {fetchPolicy: 'no-cache'})
    const [tab, setTab] = useState(0)

    const clickAboutTab = () => setTab(0);
    const clickBaseTab = () => setTab(1);

    useEffect(() => {
        getPokemonDetail({variables: { name: pokemonName }})
    }, [pokemonName, getPokemonDetail])

    return (
        <Container>
            <Offline polling={false}>
                <AutoDismiss time={3000}>
                    <Hero size="small" color="danger">
                        <Hero.Body className="has-text-centered">
                            <p className="title is-4">
                                Connect to the internet 
                            </p>
                            <p className="subtitle is-6">
                                You're offline. Check your connection.
                            </p>
                        </Hero.Body>
                    </Hero>
                </AutoDismiss>
            </Offline>
            {loading && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__Time_1337508.png" css={alignCenter} className="is-block" width="128" height="128" alt="Loading Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Loading Pokemon Detail
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {error && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={alignCenter} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Failed to Load Pokemon Detail
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {pokemon.length === 0 && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={alignCenter} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered is-capitalized">
                            pokemon <code>{pokemonName}</code> not found 
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {pokemon.length > 0 && <>
                <Hero size="small" className={`color-bg-${pokemon[0].pokemons[0].types[0].type.name}`}>
                    <Hero.Body className="has-text-centered">
                        <figure className="image is-150x150" css={alignCenter}>
                            <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon[0].id}.png`} width="96" height="96" alt={`${pokemon.name} Sprite`} loading="lazy"/>
                        </figure>
                        <div className="is-capitalized has-text-weight-medium">#{String(pokemon[0].id).padStart(3, '0')}</div>
                        <div className="is-capitalized has-text-weight-bold subtitle m-0">{pokemon[0].name}</div>
                        <div className="mt-1">
                            {pokemon[0].pokemons[0].types.map((pokemon) => 
                                <span className={`badges color-${pokemon.type.name}`} key={pokemon.type.name}>{pokemon.type.name}</span>
                            )}
                        </div>
                    </Hero.Body>
                    <Box className="is-shadowless" css={roundedBox}>
                        <div className="tabs is-centered is-fullwidth">
                            <ul>
                                <li className={tab === 0 ? "is-active" : ""} onClick={clickAboutTab}>About</li>
                                <li className={tab === 1 ? "is-active" : ""} onClick={clickBaseTab}>Base Stats</li>
                            </ul>
                        </div>
                        {tab === 0 && <> 
                            <div className="block">{pokemon[0].description[0].flavor_text}</div>
                            <div className="block">
                                <Columns breakpoint="mobile">
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Height
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        {pokemon[0].pokemons[0].height / 10} Meters
                                    </Columns.Column>
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Weight
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        {pokemon[0].pokemons[0].weight / 10} KG
                                    </Columns.Column>
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Abilities
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        {pokemon[0].pokemons[0].abilities.map((ability) => ability.ability.name).map((ability) => ability.charAt(0).toUpperCase() + ability.slice(1)).join(", ")}
                                    </Columns.Column>
                                </Columns>
                            </div>
                            <div className="block subtitle is-5" css={semiBold}>
                                Breeding
                            </div>
                            <div className="block">
                                <Columns breakpoint="mobile">
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Gender
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        <span className="pokemon-gender"><span className="gender-male">♂</span> {pokemon[0].gender_rate === -1 ? 0 : pokemon[0].gender_rate * 100 / 8}%</span>
                                        <span className="pokemon-gender"><span className="gender-female">♀</span> {pokemon[0].gender_rate === -1 ? 0 : (8 - pokemon[0].gender_rate) * 100 / 8}%</span>
                                    </Columns.Column>
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Egg Groups
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        {pokemon[0].egg_groups.map((group) => group.group.name).map((group) => group.charAt(0).toUpperCase() + group.slice(1)).join(", ")}
                                    </Columns.Column>
                                    <Columns.Column size={4} css={[greyText, semiBold]}>
                                        Egg Cycles
                                    </Columns.Column>
                                    <Columns.Column size={8} css={semiBold}>
                                        {pokemon[0].hatch_counter} (±{(255 * (pokemon[0].hatch_counter + 1)).toLocaleString()} Steps)
                                    </Columns.Column>
                                </Columns>
                            </div>
                        </>}
                        {tab === 1 &&pokemon[0].pokemons[0].stats.map((base_stat) => (
                            <div className="block">
                                <div css={spaceContentBetween}>
                                    <span css={textUppercase}>{base_stat.stat.name}</span>
                                    <span css={textUppercase}>{base_stat.base_stat}</span>
                                </div>
                                <Progress value={base_stat.base_stat} max={255} color="info" size="small" />
                            </div>
                        ))}
                    </Box>
                </Hero>
            </>}
        </Container>
    );
}