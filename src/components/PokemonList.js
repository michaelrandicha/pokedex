import React, { useEffect, useState } from 'react';
import { Container, Hero, Heading, Card, Columns } from 'react-bulma-components';
import { NavLink } from 'react-router-dom';
import { useInView } from 'react-intersection-observer';
import { Offline } from "react-detect-offline";
import { useQuery } from '@apollo/client';

import AutoDismiss from './custom/AutoDismiss';
import { spinner } from './styles/spinner';
import { alignCenter, hoverAnimation, roundedBorder } from './styles/global';
import { PokemonsQuery } from '../graphql/pokemons-query';

export default function PokemonList() {
    const [pokemonPerPage] = useState(20)
    const [page, setPage] = useState(1)
    const [ref, inView] = useInView()

    const { loading, error, data: { species: pokemons = {}, species_aggregate: { aggregate: { count: totalPokemon = 0 } = {} } = {} } = {}, fetchMore } = useQuery(PokemonsQuery.GET_POKEMONS, {
        variables: {
            limit: pokemonPerPage,
            offset: 0,
        },
    })

    useEffect(() => {
        if (page === 1) return;
        fetchMore({
            variables: {
                offset: (page - 1) * pokemonPerPage,
            }
        })
    }, [page, pokemonPerPage, fetchMore])

    useEffect(() => {
        if(inView) setPage((page) => page+1)
    }, [inView])
    
    return (
        <Container>
            <Offline polling={false}>
                <AutoDismiss time={3000}>
                    <Hero size="small" color="danger">
                        <Hero.Body className="has-text-centered">
                            <p className="title is-4">
                                Connect to the internet 
                            </p>
                            <p className="subtitle is-6">
                                You're offline. Check your connection.
                            </p>
                        </Hero.Body>
                    </Hero>
                </AutoDismiss>
            </Offline>
            {Object.keys(pokemons).length === 0 && loading && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__Time_1337508.png" css={alignCenter} className="is-block" width="128" height="128" alt="Loading Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Loading Pokemon List
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {error && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={alignCenter} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Failed to Load Pokemon List
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {Object.keys(pokemons).length !== 0 && <>
                <Columns className="is-mobile m-0 my-4">
                    {pokemons.map((pokemon) => 
                        <Columns.Column size="half" className="p-2" key={pokemon.name}>
                            <NavLink to={`/pokemon/${pokemon.name}`}>
                                <Card className={`is-shadowless color-bg-${pokemon.pokemons[0].types[0].type.name}`} css={[roundedBorder, hoverAnimation]}>
                                    <div className="card-image pt-4">
                                        <figure className="image is-150x150" css={alignCenter}>
                                            <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`} width="96" height="96" alt={`${pokemon.name} Sprite`} loading="lazy"/>
                                        </figure>
                                    </div>
                                    <Card.Content className="is-shadowless px-4 pb-4 pt-0">
                                        <div className="is-capitalized has-text-weight-medium">#{String(pokemon.id).padStart(3, '0')}</div>
                                        <div className="is-capitalized has-text-weight-bold subtitle m-0">{pokemon.name}</div>
                                        <div className="mt-1">
                                            {pokemon.pokemons[0].types.map((pokemon) => 
                                                <span className={`badges color-${pokemon.type.name}`} key={pokemon.type.name}>{pokemon.type.name}</span>
                                            )}
                                        </div>
                                    </Card.Content>
                                </Card>
                            </NavLink>
                        </Columns.Column>
                    )}
                </Columns>
                {pokemons.length < totalPokemon && <>
                    <div ref={ref} css={[spinner, alignCenter]}></div>
                    <Heading size={5} className="has-text-centered">
                        Loading More Pokemon
                    </Heading>
                </>}
            </>}
        </Container>
    );
}