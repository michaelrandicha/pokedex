import React, { useEffect, useState } from 'react';

const AutoDismiss = ({ time, children }) => {
    const [alert, setAlert] = useState(true);
        
    useEffect(() => {
        const timer = setTimeout(() => {
            setAlert(false);
        }, time);

        return () => clearTimeout(timer);
    }, [time]);

    return <>
        {alert && children}
    </>
};

export default AutoDismiss;