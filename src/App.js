import './App.scss';

import { lazy, Suspense } from 'react';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom';

import TopNavigation from './components/navigations/TopNavigation';
import { MobileView } from './components/styles/global';

const PokemonList = lazy(() => import('./components/PokemonList'));
const PokemonDetail = lazy(() => import('./components/PokemonDetail'));

export default function App() {
  return (
    <HashRouter>
      <MobileView>
        <TopNavigation></TopNavigation>
        <Suspense fallback={<div className="pageloader is-link is-active"><span className="title">Loading</span></div>}>
          <Switch>
            <Route exact path="/" component={PokemonList}></Route>
            <Route path="/pokemon/:pokemonName" component={PokemonDetail}></Route>
            <Route path="*">
              <Redirect to="/" />
            </Route>
          </Switch> 
        </Suspense>
      </MobileView>
    </HashRouter>
  )
}
